//
//  API.h
//  Login
//
//  Created by 罗贤明 on 2018/4/15.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
// API 文件， 用于定义模块的对外接口。命名规范:
// MODULE[_PAGE]_TYPE[_item]

#pragma mark - data
/**
   登录信息
 */
@protocol LoginUserInfoModelProtocol <NSObject>
@property (nonatomic,strong) NSString *account;
@property (nonatomic,strong) NSNumber *level;
@property (nonatomic,strong) NSDictionary *detailInfo;
@property (nonatomic,strong) NSArray *tagList;
@end

// 这里记录的是 login模块中 相关data 数据的 key值。

// 登录状态， Boolean类型
#define LOGIN_DATA_LOGIN                   @"login"
// 用户信息， Model类型 ， 具体结构 LoginUserInfoModelProtocol
#define LOGIN_DATA_USERINFO                @"userInfo"
// 帐号 ， String 类型。
#define LOGIN_DATA_ACCOUNT                 @"account"

#pragma mark - router

// 登录界面。 只有 跳转路由。
// 声明路由。
#define LOGIN_ROUTER_LOGIN                 @"axes://login/login"
// 可以附带参数 ，LOGIN_DATA_ACCOUNT ， 自动填写帐号
// 登录成功时，有回调, 返回数据为 { LOGIN_DATA_USERINFO : LoginUserInfoModelProtocol}
// 登录成功会发送 LOGIN_EVENT_LOGINSTATUS 通知, 并设置 LOGIN_DATA_LOGIN 和 LOGIN_DATA_USERINFO

// 注册页面， 只有跳转路由
#define LOGIN_ROUTER_REGISTER               @"axes://login/register"
// 注册成功时，有回调, 返回数据为 {LOGIN_DATA_USERINFO: LoginUserInfoModelProtocol}
// 注册成功会发送 LOGIN_EVENT_LOGINSTATUS 和 LOGIN_EVENT_REGISTERSUCCESS 通知, 并设置 LOGIN_DATA_LOGIN 和 LOGIN_DATA_USERINFO

#pragma mark - event

// 在登录成功，或者注册成功时， 会发送该通知。
#define LOGIN_EVENT_LOGINSTATUS             @"LoginStatusChange"
// 该通知附带 LOGIN_DATA_LOGIN 和 LOGIN_DATA_USERINFO 数据

// 注册成功时，会发送该通知
#define LOGIN_EVENT_REGISTERSUCCESS         @"RegistAccountSuccess"
// 该通知附带一个 LOGIN_DATA_USERINFO 数据。




