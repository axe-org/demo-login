//
//  ViewController.m
//  Test
//
//  Created by 罗贤明 on 2018/4/15.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "ViewController.h"
#import "Login.h"
#import "Axe.h"

typedef void (^TableViewItemClickBlock)(void);

@interface TableViewItem : NSObject

@property (nonatomic,strong) NSString *title;
@property (nonatomic,copy) TableViewItemClickBlock block;

@end

@implementation TableViewItem

@end

@interface ViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableArray<TableViewItem *> *cells;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _cells = [[NSMutableArray alloc] init];
    self.title = @"test";
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    // 这里添加内容
    __weak ViewController *wself = self;
    [self addCellTitle:@"login" action:^{
        [wself jumpTo:@"axes://login/login"];
    }];
    [self addCellTitle:@"register" action:^{
        [wself jumpTo:@"axes://login/register"];
    }];
    
    [_tableView reloadData];
}

- (void)addCellTitle:(NSString *)title action:(TableViewItemClickBlock)block {
    TableViewItem *item = [[TableViewItem alloc] init];
    item.title = title;
    item.block = block;
    [_cells addObject:item];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cells.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    static NSString *cellIdentifier = @"cellIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = _cells[indexPath.row].title;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_cells[indexPath.row].block) {
        _cells[indexPath.row].block();
    }
}


@end
